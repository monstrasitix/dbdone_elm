import Debug
import Html
import Html.Events as Event
import Html.Attributes as Attribute

main : Program Never Model Msg
main =
  Html.program
    { init = (model, Cmd.none)
    , update = update
    , view = view
    , subscriptions = \model -> Sub.none
    }

mainContainer : List (Html.Html Msg) -> Html.Html Msg
mainContainer body =
  Html.div []
    [ getBootstrap "darkly"
    , Html.br [][]
    , Html.div [ Attribute.class "container" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-12" ]
                [ Html.div [ Attribute.class "panel panel-default" ]
                    [ Html.div [ Attribute.class "panel-body" ]
                        body
                    ]
                ]
            ]
        ]
    ]

getBootstrap : String -> Html.Html Msg
getBootstrap theme =
  let
    attrs : List (Html.Attribute msg)
    attrs =
      [ Attribute.rel "stylesheet"
      , Attribute.type_ "text/css"
      , Attribute.href <| "http://bootswatch.com/" ++ theme ++ "/bootstrap.min.css"
      ]
  in
    Html.node "link" attrs []

fst : (a, b) -> a
fst (a, b) = a

snd : (a, b) -> b
snd (a, b) = b

type alias Css =
  List (String, String)
{-------------------------------------------------------------------------------------------------------------------}     
{-------------------------------------------------------------------------------------------------------------------}

flex : Css
flex =
  [("display","flex")]

{-------------------------------------------------------------------------------------------------------------------}     
{-------------------------------------------------------------------------------------------------------------------}    
   
   
type alias Model =
  { selectedControl : String
  , controlTypes : List (ControlType, String)
  , controls : List Control
  }
  
type alias TextSettings =
  { editing : Bool
  , labelName : String
  , isRequired : Bool
  }
  
type alias NumberSettings =
  { editing : Bool
  , labelName : String
  }

type alias Control =
  (Int, ControlType)
  
  
model : Model
model =
  { selectedControl = "Text Control"
  , controlTypes =
      [ (Text <| TextSettings False "Label" False
        , "Text Control"
        )
      , (Number <| NumberSettings False "Label"
        , "Number Control"
        )
      ]
  , controls = []
  }
  
  
{-------------------------------------------------------------------------------------------------------------------}     
{-------------------------------------------------------------------------------------------------------------------}     

  
type Msg
  = AddControl
  | SelectControl String
  | ToggleEditing Int
  | UpdateLabelName Int String
  | UpdateIsRequired Int Bool
  
type ControlType
  = Text TextSettings
  | Number NumberSettings
  
{-------------------------------------------------------------------------------------------------------------------}    
{-------------------------------------------------------------------------------------------------------------------}   

updateSelectControl : String -> Model -> Model
updateSelectControl value model =
  { model | selectedControl = value }

updateControls : List Control -> Model -> Model
updateControls list model =
  { model | controls = list }

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
  
    SelectControl value ->
      let
        log = Debug.log "SelectControl -> " value
      in
        ( model |> updateSelectControl value, Cmd.none)
        
    AddControl ->
      let
        log = Debug.log "AddControl -> " "Added"
        
        newList : List (ControlType, String)
        newList =
          model.controlTypes
            |> List.filter (\(_, name) -> name == model.selectedControl)
          
        control : Control
        control =
          case List.head newList of
            Just (a, _) -> (List.length model.controls, a)
            Nothing -> (1, Text (TextSettings False "Boom" False))
      in
        ( model
            |> updateControls (control :: model.controls)
        , Cmd.none
        )
        
    ToggleEditing id ->
      let
        log = Debug.log "ToggleEditing -> " id
        
        newList : List Control
        newList =
          List.map (toggleEdit id) model.controls
      in
        ( model |> updateControls newList
        , Cmd.none
        )
        
    UpdateLabelName id value ->
      let
        log = Debug.log "UpdateLabelName -> " id
        
        newList : List Control
        newList =
          List.map (changeLabelName id value) model.controls
        
      in
        ( model |> updateControls newList
        , Cmd.none
        )
        
    UpdateIsRequired id isRequired ->
      let
        log = Debug.log "UpdateIsRequired -> " id
        
        newList : List Control
        newList =
          List.map (changeisRequired id isRequired) model.controls
      in
        (model, Cmd.none)
        
        
changeisRequired : Int -> Bool -> Control -> Control
changeisRequired id isRequired (identity, controlType) =
  if identity == id then
    case controlType of
      Text data ->
        (identity, Text { data | isRequired = not isRequired })
      Number data ->
        (identity, controlType)
  else
    (identity, controlType)


changeLabelName : Int -> String -> Control -> Control
changeLabelName id value (identity, controlType) =
  if identity == id then
    case controlType of
      Text data ->
        (identity, Text { data | labelName = value })
      Number data ->
        (identity, Number { data | labelName = value })
  else
    (identity, controlType)

        
toggleEdit : Int -> Control -> Control
toggleEdit id (identity, controlType) =
  if identity == id then
    case controlType of
      Text data ->
        (identity, Text { data | editing = not data.editing })
      Number data ->
        (identity, Number { data | editing = not data.editing })
  else
    (identity, controlType)

      
{-------------------------------------------------------------------------------------------------------------------}         
{-------------------------------------------------------------------------------------------------------------------}         
      
      
view : Model -> Html.Html Msg
view model =
  mainContainer
    [ addControl model.controlTypes
    , Html.br [][]
    , Html.form []
        (List.map renderControlsSettings <| List.reverse model.controls)
    ]
    

addControl : List (ControlType, String) -> Html.Html Msg
addControl controlTypes =
  Html.div [ Attribute.style flex ]
    [ Html.select
        [ Attribute.class "form-control"
        , Event.onInput SelectControl
        ]
        (List.map addControlOption controlTypes)
    , Html.button
        [ Attribute.class "btn btn-primary"
        , Event.onClick AddControl
        ]
        [ Html.text "Add Control" ]
    ]


addControlOption : (ControlType, String) -> Html.Html Msg
addControlOption (controlType, name) =
  Html.option
    [ Attribute.value name ]
    [ Html.text name ]

    
textControl : TextSettings -> Html.Html Msg
textControl settings =
  Html.div [ Attribute.class "form-group btn-block" ]
    [ Html.label []
        [ Html.text settings.labelName ]
    , Html.input
        [ Attribute.class "form-control"
        , Attribute.type_ "text"
        , Attribute.required settings.isRequired
        ]
        []
    ]
    
textSettings : Int -> TextSettings -> Html.Html Msg
textSettings id settings =
  Html.div []
    [ Html.div [ Attribute.class "form-group" ]
        [ Html.label []
            [ Html.text "Label Name" ]
        , Html.input
            [ Attribute.class "form-control"
            , Attribute.type_ "text"
            , Attribute.required True
            , Attribute.value settings.labelName
            , Event.onInput <| UpdateLabelName id
            ]
            []
        ]
    , Html.div [ Attribute.class "form-group" ]
        [ Html.label []
            [ Html.text "Is Required "
            , Html.input
                [ Attribute.value <| toString settings.isRequired
                , Attribute.type_ "checkbox"
                , Attribute.name "1_checkbox"
                , Event.onClick <| UpdateIsRequired id settings.isRequired
                ]
                []
            ]
        ]
    ]


    
numberControl : NumberSettings -> Html.Html Msg
numberControl settings =
  Html.div [ Attribute.class "form-group btn-block" ]
    [ Html.label []
        [ Html.text settings.labelName ]
    , Html.input
        [ Attribute.class "form-control"
        , Attribute.type_ "number"
        ]
        []
    ]
    
numberSettings : Int -> NumberSettings -> Html.Html Msg
numberSettings id settings =
  Html.div []
    [ Html.text "I Am Number Settings"
    ]
    
  
renderControls : Control -> Html.Html Msg
renderControls (id, control) =
  case control of
    Text set ->
      textControl set
    Number set ->
      numberControl set
      
renderControlsSettings : Control -> Html.Html Msg
renderControlsSettings (id, control) =
  case control of
    Text set ->
      settings id set textControl textSettings
    Number set ->
      settings id set numberControl numberSettings
      
      
--settings : a -> (a -> Html.Html Msg) -> (a -> Html.Html Msg) -> Html.Html Msg
settings id set control controlSettings =
  Html.div []
    [ Html.div [ Attribute.style <| flex ++ [("align-items", "center")] ]
        [ control set
        , Html.button
            [ Attribute.class "btn btn-info"
            , Attribute.style [("margin-top", "10px")]
            , Attribute.type_ "button"
            , Event.onClick <| ToggleEditing id
            ]
            [ Html.text "Settings"]
        ]
    , Html.div
        [ Attribute.class "panel panel-default"
        , Attribute.style [("display", displaySettings set.editing)]
        ]
        [ Html.div [ Attribute.class "panel-body" ]
            [ controlSettings id set
            ]
        ]
    ]

displaySettings : Bool -> String
displaySettings isEditing =
  case isEditing of
    True -> "block"
    False -> "none"
   
