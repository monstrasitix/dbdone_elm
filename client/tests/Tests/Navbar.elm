module Tests.Navbar exposing (..)

import Test exposing (..)
import Expect
import Navbar.View as View
import Navbar.Types as Types
import Navbar.Update as Update
import String


m : Types.Model
m =
    Types.model


tests : Test
tests =
    describe "Module: Navbar"
        [ describe "Navbar View"
            [ test "toggleSidebar: Collapsed" <|
                \() ->
                    let
                        doesContain : Bool
                        doesContain =
                            String.contains "collapsed" <| View.toggleSidebar True
                    in
                        Expect.equal True doesContain
            , test "toggleSidebar: Collapse" <|
                \() ->
                    let
                        doesContain : Bool
                        doesContain =
                            String.contains "collapse" <| View.toggleSidebar False
                    in
                        Expect.equal True doesContain
            ]
        , describe "Navbar Update"
            [ test "updateIsToggled" <|
                \() ->
                    let
                        oldModel : Types.Model
                        oldModel =
                            { m | isToggled = False }

                        newModel : Types.Model
                        newModel =
                            Update.updateIsToggled False Types.model
                    in
                        Expect.notEqual oldModel.isToggled newModel.isToggled
            , test "toggleLink: Matching href's" <|
                \() ->
                    let
                        href : String
                        href =
                            "#test1"

                        isActive : Bool
                        isActive =
                            False

                        link : Types.Link
                        link =
                            Types.Link isActive "Test 1" href

                        result : Bool
                        result =
                            .active (Update.toggleLink href link)
                    in
                        Expect.equal (not isActive) result
            , test "toggleLink: Not matching href's" <|
                \() ->
                    let
                        href : String
                        href =
                            "#test2"

                        isActive : Bool
                        isActive =
                            False

                        link : Types.Link
                        link =
                            Types.Link isActive "Test 2" "#different_href"

                        result : Bool
                        result =
                            .active (Update.toggleLink href link)
                    in
                        Expect.equal isActive result
            , test "toggleLinkList: " <|
                \() ->
                    let
                        links : List Types.Link
                        links =
                            [ Types.Link False "Link 1" "#1"
                            ]

                        href : String
                        href =
                            "#1"

                        result : List Types.Link
                        result =
                            Update.toggleLinkList href links

                    in
                        Expect.notEqual links result
            ]
        ]
