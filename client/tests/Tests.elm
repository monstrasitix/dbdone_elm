module Tests exposing (..)

import Test
import Tests.Navbar as Navbar

all : Test.Test 
all =
    Test.concat
        [ Navbar.tests
        ]