module Update exposing (..)

import Types as Data exposing (Model, Msg)
import Routing
import Navbar.Update as Navbar
import Home.Update as Home
import Authentication.Update as Authentication
import Dashboard.Update as Dashboard
import Structure.Update as Structure
import Records.Update as Records


-- Main Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Parse The Route When Location Is Changed
        Data.OnLocationChange location ->
            let
                newRoute : Routing.Route
                newRoute =
                    Routing.parseLocation location
            in
                ( { model | route = newRoute }
                , Cmd.none
                )

        -- Bind Navbar Messages
        Data.NavbarMessage message ->
            let
                ( newModel, newCmd ) =
                    Navbar.update message model.navbarModel
            in
                ( { model | navbarModel = newModel }
                , Cmd.map Data.NavbarMessage newCmd
                )

        -- Bind Dashboard Messages
        Data.DashboardMessage message ->
            let
                ( newModel, newCmd ) =
                    Dashboard.update message model.dashboardModel
            in
                ( { model | dashboardModel = newModel }
                , Cmd.map Data.DashboardMessage newCmd
                )

        -- Bind Home Messages
        Data.HomeMessage message ->
            let
                ( newModel, newCmd ) =
                    Home.update message model.homeModel
            in
                ( { model | homeModel = newModel }
                , Cmd.map Data.HomeMessage newCmd
                )

        -- Bind Authentication Messages
        Data.AuthenticationMessage message ->
            let
                ( newModel, newCmd ) =
                    Authentication.update message model.authenticationModel
            in
                ( { model | authenticationModel = newModel }
                , Cmd.map Data.AuthenticationMessage newCmd
                )

        -- Bind Structure Messages
        Data.StructureMessage message ->
            let
                ( newModel, newCmd ) =
                    Structure.update message model.structureModel
            in
                ( { model | structureModel = newModel }
                , Cmd.map Data.StructureMessage newCmd
                )

        -- Bind Records Messages
        Data.RecordsMessage message ->
            let
                ( newModel, newCmd ) =
                    Records.update message model.recordsModel
            in
                ( { model | recordsModel = newModel }
                , Cmd.map Data.RecordsMessage newCmd
                )
