module Records.View exposing (..)

import Records.Types as Data exposing (Model, Msg)
import Forms.View as Forms
import Html
import Html.Attributes as Attribute
import Forms.View as Control


-- Records View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "container padding-top" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-12" ]
                [ Html.div [ Attribute.class "panel panel-default" ]
                    [ Html.div []
                        [ viewTabs model
                        ]
                    ]
                , Html.div [ Attribute.class "panel panel-default" ]
                    [ Html.div []
                        [ tabContent model
                        ]
                    ]
                ]
            ]
        ]



-- Record Tabs


viewTabs : Model -> Html.Html Msg
viewTabs model =
    Html.ul [ Attribute.class "nav nav-tabs" ]
        [ Html.li [ Attribute.class "active" ]
            [ Html.a
                [ Attribute.href "#records"
                , Attribute.attribute "data-toggle" "tab"
                ]
                [ Html.text "Records" ]
            ]
        , Html.li [ Attribute.class "" ]
            [ Html.a
                [ Attribute.href "#editForm"
                , Attribute.attribute "data-toggle" "tab"
                ]
                [ Html.text "Edit Form" ]
            ]
        ]



-- Record Tab Contents


tabContent : Model -> Html.Html Msg
tabContent model =
    Html.div [ Attribute.class "tab-content" ]
        [ Html.div [ Attribute.id "records", Attribute.class "tab-pane fade in active" ]
            [ records model ]
        , Html.div [ Attribute.id "editForm", Attribute.class "tab-pane fade" ]
            [ forms model ]
        ]



-- Table With Records


records : Model -> Html.Html Msg
records model =
    Html.div [ Attribute.class "panel-body" ]
        [ Html.table [ Attribute.class "table" ]
            [ Html.thead []
                [ Html.tr []
                    [ Html.th [] [ Html.text "Column 1" ]
                    , Html.th [] [ Html.text "Column 2" ]
                    , Html.th [] [ Html.text "Column 3" ]
                    , Html.th [] [ Html.text "Column 4" ]
                    , Html.th [] [ Html.text "Column 5" ]
                    ]
                ]
            , Html.tbody []
                (List.map temp_row [ 1, 2, 3, 4, 5, 6, 7 ])
            ]
        ]


temp_row : Int -> Html.Html Msg
temp_row number =
    Html.tr []
        [ Html.td [] [ Html.text <| "Row Data " ++ (toString number) ]
        , Html.td [] [ Html.text <| "Row Data " ++ (toString number) ]
        , Html.td [] [ Html.text <| "Row Data " ++ (toString number) ]
        , Html.td [] [ Html.text <| "Row Data " ++ (toString number) ]
        , Html.td [] [ Html.text <| "Row Data " ++ (toString number) ]
        ]



-- Edit Form


forms : Model -> Html.Html Msg
forms model =
    Html.map Data.FormsMessage <|
        Forms.editView model.formsModel
