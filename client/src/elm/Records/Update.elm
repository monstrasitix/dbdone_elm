module Records.Update exposing (..)

import Records.Types as Data exposing (Model, Msg)
import Forms.Update as Forms


-- Records Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Bind Forms
        Data.FormsMessage message ->
            let
                ( newModel, newCmd ) =
                    Forms.update message model.formsModel
            in
                ( { model | formsModel = newModel }
                , Cmd.map Data.FormsMessage newCmd
                )
