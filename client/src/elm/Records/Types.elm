module Records.Types exposing (..)

import Forms.Types as Forms
-- Records Messages


type Msg
    = FormsMessage Forms.Msg



-- Records Model Schema


type alias Model =
    { formsModel : Forms.Model
    }



-- Records Model


model : Model
model =
    { formsModel = Forms.model
    }
