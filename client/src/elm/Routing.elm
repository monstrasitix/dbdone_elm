module Routing exposing (..)

import UrlParser exposing (..)
import Navigation


-- Routes


type Route
    = Route_Home
    | Route_Dashboard
    | Route_Authentication_Login
    | Route_Authentication_Register
    | Route_Structure_Storage Int
    | Route_Structure_Category Int
    | Route_Records Int
    | Route_None



-- Authentication Routes


type Authentication
    = Route_Login
    | Route_Register



-- Structure Routes


type Structure
    = Route_Storage
    | Route_Category



-- Route Matching


matchers : UrlParser.Parser (Route -> a) a
matchers =
    UrlParser.oneOf
        [ UrlParser.map Route_Home top
        , UrlParser.map Route_Dashboard (s "dashboard")
        , UrlParser.map Route_Authentication_Login (s "login")
        , UrlParser.map Route_Authentication_Register (s "register")
        , UrlParser.map Route_Structure_Storage (s "storages" </> UrlParser.int)
        , UrlParser.map Route_Structure_Category (s "categories" </> UrlParser.int)
        , UrlParser.map Route_Records (s "records" </> UrlParser.int)
        ]



-- Location Parsing To Routes


parseLocation : Navigation.Location -> Route
parseLocation location =
    case (UrlParser.parseHash matchers location) of
        -- Route Parsed
        Just route ->
            route

        -- No Route
        Nothing ->
            Route_None
