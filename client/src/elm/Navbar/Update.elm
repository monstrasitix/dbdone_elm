module Navbar.Update exposing (..)

import Navbar.Types as Data exposing (Model, Msg)


-- Navbar Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Sidebar Toggling
        Data.ToggleSidebar toggled ->
            ( model
                |> updateIsToggled toggled
            , Cmd.none
            )

        -- Redirecting
        Data.Redirect href ->
            ( model
                |> updateIsToggled True
                |> updateLinks
                    (toggleLinkList href model.links)
            , Cmd.none
            )



-- Updates isToggled Property


updateIsToggled : Bool -> Model -> Model
updateIsToggled isToggled model =
    { model | isToggled = not isToggled }



-- Updates links Property


updateLinks : List Data.Link -> Model -> Model
updateLinks links model =
    { model | links = links }



-- Toggles A Link


toggleLink : String -> Data.Link -> Data.Link
toggleLink href link =
    case link.href == href of
        True ->
            { link | active = True }

        False ->
            { link | active = False }


toggleLinkList : String -> List Data.Link -> List Data.Link
toggleLinkList href links =
    List.map (toggleLink href) links
