module Navbar.Types exposing (..)

-- Navbar Messages


type Msg
    = ToggleSidebar Bool
    | Redirect String



-- Navbar Model Schema


type alias Model =
    { isToggled : Bool
    , links : List Link
    }



-- Navbar Link Schema


type alias Link =
    { active : Bool
    , name : String
    , href : String
    }



-- Navbar Model


model : Model
model =
    { isToggled = False
    , links =
        [ Link True "Home" "/#"
        , Link False "Dashboard" "/#dashboard"
        , Link False "Storages" "/#storages/1"
        , Link False "Login" "/#login"
        , Link False "Register" "/#register"
        ]
    }
