module Navbar.View exposing (..)

import Navbar.Types as Data exposing (Model, Msg)
import Html
import Html.Events as Event
import Html.Attributes as Attribute


-- Toggling Button


togglingButton : Bool -> Html.Html Msg
togglingButton isToggled =
    Html.button
        [ Attribute.type_ "button"
        , Attribute.class "navbar-toggle collapsed"
        , Event.onClick <| Data.ToggleSidebar isToggled
        ]
        [ Html.span [ Attribute.class "sr-only" ]
            [ Html.text "Toggle navigation" ]
        , Html.span [ Attribute.class "icon-bar" ] []
        , Html.span [ Attribute.class "icon-bar" ] []
        , Html.span [ Attribute.class "icon-bar" ] []
        ]



-- Sidebar Toggling


toggleSidebar : Bool -> String
toggleSidebar isToggled =
    case isToggled of
        True ->
            "aside collapsed"

        False ->
            "aside collapse"



-- Navbar Brand


viewBrand : Html.Html Msg
viewBrand =
    Html.a
        [ Attribute.class "navbar-brand"
        , Attribute.href "#"
        , Event.onClick <|
            Data.Redirect "/#"
        ]
        [ Html.text "DB Done" ]



-- Sidebar


sidebar : Model -> Html.Html Msg
sidebar model =
    Html.div
        [ Attribute.class <|
            toggleSidebar model.isToggled
        ]
        [ Html.ul [ Attribute.class "aside__list list-unstyled" ]
            (List.map sidebarLink model.links)
        ]



-- Sidebar Link


sidebarLink : Data.Link -> Html.Html Msg
sidebarLink link =
    Html.a
        [ Attribute.href link.href
        , Attribute.class "aside__list-item"
        , Attribute.classList
            [ ( "active", link.active )
            , ( "", not link.active )
            ]
        , Event.onClick <|
            Data.Redirect link.href
        ]
        [ Html.text link.name ]



-- Navbar View Function


view : Model -> Html.Html Msg
view model =
    Html.nav [ Attribute.class "navbar navbar-default navbar-fixed-top" ]
        [ Html.div [ Attribute.class "container-fluid" ]
            [ Html.div [ Attribute.class "navbar-header" ]
                [ togglingButton model.isToggled
                , viewBrand
                ]
            ]
        , sidebar model
        ]
