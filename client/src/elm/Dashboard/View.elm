module Dashboard.View exposing (..)

import Dashboard.Types as Data exposing (Model, Msg)
import Html
import Html.Attributes as Attribute


-- Dashboard View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "padding-top container" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-4" ]
                [ panel "Storage 1" model
                ]
            , Html.div [ Attribute.class "col-xs-4" ]
                [ panel "Storage 2" model
                ]
            , Html.div [ Attribute.class "col-xs-4" ]
                [ panel "Storage 3" model
                ]
            ]
        , Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-12" ]
                [ panel "Statistics" model
                ]
            ]
        , Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-xs-6" ]
                [ panel "Category 1" model
                ]
            , Html.div [ Attribute.class "col-xs-6" ]
                [ panel "Category 2" model
                ]
            ]
        ]



-- Dashboard Panel


panel : String -> Model -> Html.Html Msg
panel heading model =
    Html.div [ Attribute.class "panel panel-default" ]
        [ Html.div [ Attribute.class "panel-heading" ]
            [ Html.text heading
            ]
        , Html.div [ Attribute.class "panel-body" ]
            [ Html.text "This is the storaeg"
            ]
        ]
