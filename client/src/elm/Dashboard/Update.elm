module Dashboard.Update exposing (..)

import Dashboard.Types as Data exposing (Model, Msg)


-- Dashboard Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- None
        Data.None ->
            ( model, Cmd.none )
