module Home.Update exposing (..)

import Home.Types as Data exposing (Model, Msg)


-- Home Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- None
        Data.None ->
            ( model, Cmd.none )
