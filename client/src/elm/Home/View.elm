module Home.View exposing (..)

import Home.Types as Data exposing (Model, Msg)
import Html
import Html.Attributes as Attribute


-- Home View Function


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ jumbotron
        ]



-- Home Jumbotron


jumbotron : Html.Html Msg
jumbotron =
    Html.div [ Attribute.class "jumbotron" ]
        [ Html.div [ Attribute.class "container" ]
            []
        ]
