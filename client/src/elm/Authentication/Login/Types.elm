module Authentication.Login.Types exposing (..)

-- Authentication Login Messages


type Msg
    = UsernameInput String
    | PasswordInput String



-- Authentication Login Model Schema


type alias Model =
    { username : String
    , password : String
    }



-- Authentication Login Model


model : Model
model =
    { username = ""
    , password = ""
    }
