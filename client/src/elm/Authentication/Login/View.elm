module Authentication.Login.View exposing (..)

import Authentication.Login.Types as Data exposing (Model, Msg)
import Html
import Html.Events as Event
import Html.Attributes as Attribute


-- Authentication Login View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "container" ]
        [ Html.div [ Attribute.class "row" ]
            [ Html.div [ Attribute.class "col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12" ]
                [ Html.h3 [ Attribute.class "page-header text-center" ]
                    [ Html.text "Login Form" ]
                , Html.div [ Attribute.class "panel panel-default" ]
                    [ Html.div [ Attribute.class "panel-body" ]
                        [ renderForm model
                        ]
                    ]
                ]
            ]
        ]



-- Authentication Login Form


renderForm : Model -> Html.Html Msg
renderForm model =
    Html.form []
        [ field "Username" "username" "text" model.username Data.UsernameInput
        , field "Password" "password" "password" model.password Data.PasswordInput
        , Html.div [ Attribute.class "form-group col-xs-12" ]
            [ Html.button
                [ Attribute.class "btn btn-default btn-block"
                , Attribute.type_ "button"
                ]
                [ Html.text "Login" ]
            ]
        ]



-- Render A Field


field : String -> String -> String -> String -> (String -> Msg) -> Html.Html Msg
field text id type_ value message =
    Html.div [ Attribute.class "form-group col-xs-12" ]
        [ Html.label [ Attribute.for id ]
            [ Html.text text ]
        , Html.input
            [ Attribute.id id
            , Attribute.name id
            , Attribute.class "form-control"
            , Attribute.type_ type_
            , Attribute.value value
            , Event.onInput message
            ]
            []
        ]
