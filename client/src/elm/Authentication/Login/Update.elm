module Authentication.Login.Update exposing (..)

import Authentication.Login.Types as Data exposing (Model, Msg)


-- Authentication Login Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Login Username Input
        Data.UsernameInput input ->
            ( model |> updateUsername input, Cmd.none )

        -- Login Password Input
        Data.PasswordInput input ->
            ( model |> updatePassword input, Cmd.none )



-- Updating Username


updateUsername : String -> Model -> Model
updateUsername input model =
    { model | username = input }



-- Updating Password


updatePassword : String -> Model -> Model
updatePassword input model =
    { model | password = input }
