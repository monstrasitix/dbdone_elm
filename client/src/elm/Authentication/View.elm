module Authentication.View exposing (..)

import Routing
import Authentication.Types as Data exposing (Model, Msg)
import Authentication.Login.View as Login
import Authentication.Register.View as Register
import Html


-- Authentication View Function


view : Model -> Routing.Authentication -> Html.Html Msg
view model route =
    Html.div []
        [ page model route
        ]



-- Render View By Route


page : Model -> Routing.Authentication -> Html.Html Msg
page model route =
    case route of
        -- Authentication Login Route
        Routing.Route_Login ->
            Html.map Data.LoginMessage <|
                Login.view model.loginModel

        -- Authentication Register Route
        Routing.Route_Register ->
            Html.map Data.RegisterMessage <|
                Register.view model.registerModel
