module Authentication.Types exposing (..)

import Authentication.Login.Types as Login
import Authentication.Register.Types as Register


-- Authentication Messages


type Msg
    = LoginMessage Login.Msg
    | RegisterMessage Register.Msg



-- Authentication Model Schema


type alias Model =
    { loginModel : Login.Model
    , registerModel : Register.Model
    }



-- Authentication Model


model : Model
model =
    { loginModel = Login.model
    , registerModel = Register.model
    }
