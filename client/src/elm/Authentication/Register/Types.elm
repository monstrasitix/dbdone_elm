module Authentication.Register.Types exposing (..)

-- Authentication Register Messages


type Msg
    = UsernameInput String
    | EmailInput String
    | PasswordInput String
    | RepeatPasswordInput String



-- Authentication Register Model Schema


type alias Model =
    { username : String
    , email : String
    , password : String
    , repeatPassword : String
    }



-- Authentication Register Model


model : Model
model =
    { username = ""
    , email = ""
    , password = ""
    , repeatPassword = ""
    }
