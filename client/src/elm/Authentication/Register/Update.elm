module Authentication.Register.Update exposing (..)

import Authentication.Register.Types as Data exposing (Model, Msg)


-- Authentication Register Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Registration Username Input
        Data.UsernameInput input ->
            ( model |> updateUsername input, Cmd.none )

        -- Registration Email Input
        Data.EmailInput input ->
            ( model |> updateEmail input, Cmd.none )

        -- Registration Password Input
        Data.PasswordInput input ->
            ( model |> updatePassword input, Cmd.none )

        -- Registration Repeated Password Input
        Data.RepeatPasswordInput input ->
            ( model |> updateRepeatPassword input, Cmd.none )



-- Updating Username


updateUsername : String -> Model -> Model
updateUsername input model =
    { model | username = input }



-- Updating Email


updateEmail : String -> Model -> Model
updateEmail input model =
    { model | email = input }



-- Updating Password


updatePassword : String -> Model -> Model
updatePassword input model =
    { model | password = input }



-- Updating Repeated Password


updateRepeatPassword : String -> Model -> Model
updateRepeatPassword input model =
    { model | repeatPassword = input }
