module Authentication.Update exposing (..)

import Authentication.Types as Data exposing (Model, Msg)
import Authentication.Login.Update as Login
import Authentication.Register.Update as Register


-- Authentication Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Bind Authentication Login Messages
        Data.LoginMessage message ->
            let
                ( newModel, newCmd ) =
                    Login.update message model.loginModel
            in
                ( { model | loginModel = newModel }
                , Cmd.map Data.LoginMessage newCmd
                )

        -- Bind Authentication Register Messages
        Data.RegisterMessage message ->
            let
                ( newModel, newCmd ) =
                    Register.update message model.registerModel
            in
                ( { model | registerModel = newModel }
                , Cmd.map Data.RegisterMessage newCmd
                )
