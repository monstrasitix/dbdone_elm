module Forms.Controls.Dropdown exposing (..)

import Forms.Types as Data exposing (Model, Msg)
import Forms.Controls.Fields as Field
import Html
import Html.Attributes as Attribute


-- Number Control


dropdownControl : Data.DropdownSettings -> Html.Html Msg
dropdownControl settings =
    Html.div [ Attribute.class "form-group btn-block component-input settings-control" ]
        [ Html.label []
            [ Html.text settings.labelName ]
        , Html.select
            [ Attribute.class "form-control"
            ]
            (List.map dropdownControlOptions settings.options)
        ]

dropdownControlOptions : String -> Html.Html Msg
dropdownControlOptions option =
    Html.option
        [ Attribute.value option ]
        [ Html.text option ]




-- Number Control Settings


dropdownSettings : Data.ControlId -> Data.DropdownSettings -> Html.Html Msg
dropdownSettings controlId settings =
    Html.div []
        [ Field.labelName controlId settings.labelName
        ]
