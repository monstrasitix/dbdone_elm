module Forms.Controls.Number exposing (..)

import Forms.Types as Data exposing (Model, Msg)
import Forms.Controls.Fields as Field
import Html
import Html.Attributes as Attribute


-- Number Control


numberControl : Data.NumberSettings -> Html.Html Msg
numberControl settings =
    Html.div [ Attribute.class "form-group btn-block component-input settings-control" ]
        [ Html.label []
            [ Html.text settings.labelName ]
        , Html.input
            [ Attribute.class "form-control"
            , Attribute.type_ "number"
            , Attribute.min <| toString settings.minimumValue
            , Attribute.max <| toString settings.maximumValue
            ]
            []
        ]



-- Number Control Settings


numberSettings : Data.ControlId -> Data.NumberSettings -> Html.Html Msg
numberSettings controlId settings =
    Html.div []
        [ Field.labelName controlId settings.labelName
        , Field.minimumValue controlId settings.minimumValue
        , Field.maximumValue controlId settings.maximumValue
        , Field.isRequired controlId settings.isRequired
        ]
