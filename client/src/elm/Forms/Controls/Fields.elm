module Forms.Controls.Fields exposing (..)

import Forms.Types as Data exposing (Model, Msg)
import Html
import Html.Events as Event
import Html.Attributes as Attribute


-- LabelName Field


labelName : Data.ControlId -> String -> Html.Html Msg
labelName controlId labelName =
    Html.div [ Attribute.class "form-group col-xs-12" ]
        [ Html.label [] [ Html.text "Label Name" ]
        , Html.input
            [ Attribute.class "form-control"
            , Attribute.value labelName
            , Attribute.required True
            , Attribute.type_ "text"
            , Event.onInput <| Data.UpdateLabelName controlId
            ]
            []
        ]



-- IsRequired Field


isRequired : Data.ControlId -> Bool -> Html.Html Msg
isRequired controlId isRequired =
    Html.div [ Attribute.class "checkbox col-xs-12" ]
        [ Html.label []
            [ Html.input
                [ Attribute.value <| toString isRequired
                , Attribute.type_ "checkbox"
                , Event.onClick <| Data.UpdateIsRequired controlId isRequired
                ]
                []
            , Html.text "Required"
            ]
        ]



-- Minimum Value Field


minimumValue : Data.ControlId -> Int -> Html.Html Msg
minimumValue controlId minVal =
    Html.div [ Attribute.class "form-group col-xs-6" ]
        [ Html.label [] [ Html.text "Minimum Value" ]
        , Html.input
            [ Attribute.class "form-control"
            , Attribute.value <| toString minVal
            , Attribute.type_ "number"
            , Event.onInput <| Data.UpdateMinimumValue controlId
            ]
            []
        ]



-- Maximum Value


maximumValue : Data.ControlId -> Int -> Html.Html Msg
maximumValue controlId maxVal =
    Html.div [ Attribute.class "form-group col-xs-6" ]
        [ Html.label [] [ Html.text "Maximum Value" ]
        , Html.input
            [ Attribute.class "form-control"
            , Attribute.value <| toString maxVal
            , Attribute.type_ "number"
            , Event.onInput <| Data.UpdateMaximumValue controlId
            ]
            []
        ]
