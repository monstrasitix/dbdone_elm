module Forms.Controls.Text exposing (..)

import Forms.Types as Data exposing (Model, Msg)
import Forms.Controls.Fields as Field
import Html
import Html.Attributes as Attribute


-- Text Control


textControl : Data.TextSettings -> Html.Html Msg
textControl settings =
    Html.div [ Attribute.class "form-group btn-block component-input settings-control" ]
        [ Html.label []
            [ Html.text settings.labelName ]
        , Html.input
            [ Attribute.class "form-control"
            , Attribute.type_ "text"
            , Attribute.required settings.isRequired
            ]
            []
        ]



-- Text Control Settings


textSettings : Data.ControlId -> Data.TextSettings -> Html.Html Msg
textSettings controlId settings =
    Html.div []
        [ Field.labelName controlId settings.labelName
        , Field.isRequired controlId settings.isRequired
        ]
