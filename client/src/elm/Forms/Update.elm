module Forms.Update exposing (..)

import Forms.Types as Data exposing (Model, Msg)


-- Forms Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Adding Control To The Form
        Data.AddControl ->
            let
                -- Filtering Available Control Types To Get The Control Type
                newList : List ( Data.ControlType, String )
                newList =
                    model.controlTypes
                        |> List.filter (\( _, name ) -> name == model.selectedControl)

                -- Extracting That Control Type
                control : Data.Control
                control =
                    case List.head newList of
                        Just ( controlType, _ ) ->
                            ( List.length model.controls, controlType )

                        Nothing ->
                            ( 1, Data.Text Data.textDefault )
            in
                ( model |> updateControls (control :: model.controls)
                , Cmd.none
                )

        -- Saving The Selected Control
        Data.SelectControl selectedControl ->
            ( model |> updatedSelectedControl selectedControl
            , Cmd.none
            )

        -- Toggling Edit More For The Control
        Data.ToggleEditing controlId ->
            let
                -- Toggling The Specified Control
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.map (toggleEdit controlId)
            in
                ( model |> updateControls newList
                , Cmd.none
                )

        -- Saving Label's Name
        Data.UpdateLabelName controlId name ->
            let
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.map (changeLabelName controlId name)
            in
                ( model |> updateControls newList
                , Cmd.none
                )

        -- Saving Checkbox's Value : Is Required
        Data.UpdateIsRequired controlId isRequired ->
            let
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.map (changeIsRequired controlId isRequired)
            in
                ( model |> updateControls newList
                , Cmd.none
                )

        -- Updating Maximum Value
        Data.UpdateMaximumValue controlId value ->
            let
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.map (updateMaximumValue controlId value)
            in
                ( model |> updateControls newList
                , Cmd.none
                )

        -- Updating Minimum Value
        Data.UpdateMinimumValue controlId value ->
            let
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.map (updateMinimumValue controlId value)
            in
                ( model |> updateControls newList
                , Cmd.none
                )

        Data.ChangeControl controlId controlType ->
            let
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.map (changeControl controlId controlType)
            in
                ( model |> updateControls newList
                , Cmd.none
                )

        Data.DeleteControl controlId ->
            let
                newList : List Data.Control
                newList =
                    model.controls
                        |> List.filter (\( id, _ ) -> (/=) id controlId)
            in
                ( model |> updateControls newList
                , Cmd.none
                )


changeControl : Data.ControlId -> Data.ControlType -> Data.Control -> Data.Control
changeControl controlId controlType ( id, controlType2 ) =
    if controlId == id then
        ( id, controlType )
    else
        ( id, controlType2 )


updatedSelectedControl : String -> Model -> Model
updatedSelectedControl value model =
    { model | selectedControl = value }


updateControls : List Data.Control -> Model -> Model
updateControls list model =
    { model | controls = list }


toggleEdit : Data.ControlId -> Data.Control -> Data.Control
toggleEdit controlId ( id, controlType ) =
    if controlId == id then
        case controlType of
            Data.Text settings ->
                ( id, Data.Text { settings | isEditing = not settings.isEditing } )

            Data.Number settings ->
                ( id, Data.Number { settings | isEditing = not settings.isEditing } )

            Data.Dropdown settings ->
                ( id, Data.Dropdown { settings | isEditing = not settings.isEditing } )
    else
        ( id, controlType )


changeLabelName : Data.ControlId -> String -> Data.Control -> Data.Control
changeLabelName controlId value ( id, controlType ) =
    if controlId == id then
        case controlType of
            Data.Text settings ->
                ( id, Data.Text { settings | labelName = value } )

            Data.Number settings ->
                ( id, Data.Number { settings | labelName = value } )

            Data.Dropdown settings ->
                ( id, Data.Dropdown { settings | labelName = value } )
    else
        ( id, controlType )


changeIsRequired : Data.ControlId -> Bool -> Data.Control -> Data.Control
changeIsRequired controlId required ( id, controlType ) =
    if controlId == id then
        case controlType of
            Data.Text settings ->
                ( id, Data.Text { settings | isRequired = not required } )

            Data.Number settings ->
                ( id, Data.Number { settings | isRequired = not required } )

            Data.Dropdown settings ->
                ( id, controlType )
    else
        ( id, controlType )


updateMinimumValue : Data.ControlId -> String -> Data.Control -> Data.Control
updateMinimumValue controlId value ( id, controlType ) =
    if controlId == id then
        case controlType of
            Data.Text settings ->
                ( id, controlType )

            Data.Number settings ->
                ( id
                , Data.Number
                    { settings
                        | minimumValue =
                            case String.toInt value of
                                Ok number ->
                                    number

                                Err number ->
                                    0
                    }
                )

            Data.Dropdown settings ->
                ( id, controlType )
    else
        ( id, controlType )


updateMaximumValue : Data.ControlId -> String -> Data.Control -> Data.Control
updateMaximumValue controlId value ( id, controlType ) =
    if controlId == id then
        case controlType of
            Data.Text settings ->
                ( id, controlType )

            Data.Number settings ->
                ( id
                , Data.Number
                    { settings
                        | maximumValue =
                            case String.toInt value of
                                Ok number ->
                                    number

                                Err number ->
                                    0
                    }
                )

            Data.Dropdown settings ->
                ( id, controlType )
    else
        ( id, controlType )
