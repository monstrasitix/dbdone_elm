module Forms.View exposing (..)

import Forms.Types as Data exposing (Model, Msg)
import Forms.Controls.Text as TextControl
import Forms.Controls.Number as NumberControl
import Forms.Controls.Number as NumberControl
import Forms.Controls.Dropdown as DropdownControl
import Html
import Html.Events as Event
import Html.Attributes as Attribute


-- Form View When Inserting Records


insertView : Model -> Html.Html Msg
insertView model =
    Html.div []
        [ Html.text "Insert View"
        ]



-- Form View When Creating The Form


editView : Model -> Html.Html Msg
editView model =
    Html.div [ Attribute.class "panel-body" ]
        [ Html.div [ Attribute.class "col-xs-8 col-xs-offset-2" ]
            [ addControl model.controlTypes
            , Html.br [] []
            , Html.input
                [ Attribute.type_ "text"
                , Attribute.class "form-control"
                ]
                []
            , Html.form []
                (List.map (renderControlSettings model.controlTypes) <| List.reverse model.controls)
            ]
        ]



-- Adding A Control To The Form


addControl : List ( Data.ControlType, String ) -> Html.Html Msg
addControl controlTypes =
    Html.div [ Attribute.class "flex flex-between" ]
        [ Html.select
            [ Attribute.class "form-control component-input"
            , Event.onInput Data.SelectControl
            ]
            (List.map addControlOption controlTypes)
        , Html.button
            [ Attribute.class "btn btn-primary"
            , Event.onClick Data.AddControl
            ]
            [ Html.text "Add Control" ]
        ]


addControlOption : ( Data.ControlType, String ) -> Html.Html Msg
addControlOption ( controlType, name ) =
    Html.option
        [ Attribute.value name ]
        [ Html.text name ]



-- Renders The Control


renderControl : Data.Control -> Html.Html Msg
renderControl ( controlid, control ) =
    case control of
        Data.Text settings ->
            TextControl.textControl settings

        Data.Number settings ->
            NumberControl.numberControl settings

        Data.Dropdown settings ->
            DropdownControl.dropdownControl settings



-- Renders The Control With Settings


renderControlSettings : List ( Data.ControlType, String ) -> Data.Control -> Html.Html Msg
renderControlSettings controlTypes ( controlId, control ) =
    case control of
        Data.Text settings ->
            renderSettings controlTypes controlId settings TextControl.textControl TextControl.textSettings

        Data.Number settings ->
            renderSettings controlTypes controlId settings NumberControl.numberControl NumberControl.numberSettings

        Data.Dropdown settings ->
            renderSettings controlTypes controlId settings DropdownControl.dropdownControl DropdownControl.dropdownSettings



-- Provides Control Toggling And Delete Functionalities


renderSettings :
    List ( Data.ControlType, String )
    -> Data.ControlId
    -> { a | isEditing : Bool }
    -> ({ a | isEditing : Bool } -> Html.Html Msg)
    -> (Data.ControlId -> { a | isEditing : Bool } -> Html.Html Msg)
    -> Html.Html Msg
renderSettings controlTypes controlId settings controlFunction settingsFunction =
    Html.div []
        [ Html.div [ Attribute.class "settings" ]
            [ controlFunction settings
            , settingButtons controlTypes controlId
            ]
        , Html.div
            [ Attribute.class "panel panel-default"
            , Attribute.classList
                [ ( "show", settings.isEditing )
                , ( "hidden", not settings.isEditing )
                ]
            ]
            [ Html.div [ Attribute.class "panel-body" ]
                [ settingsFunction controlId settings
                ]
            ]
        ]



-- Setting Buttons


settingButtons : List ( Data.ControlType, String ) -> Data.ControlId -> Html.Html Msg
settingButtons controlTypes controlId =
    Html.div [ Attribute.class "btn-group" ]
        [ Html.button
            [ Attribute.class "btn btn-success"
            , Attribute.type_ "button"
            , Event.onClick <| Data.ToggleEditing controlId
            ]
            [ Html.span [ Attribute.class "glyphicon glyphicon-cog" ] [] ]
        , Html.div [ Attribute.class "btn-group" ]
            [ Html.button
                [ Attribute.class "btn btn-info dropdown-toggle"
                , Attribute.type_ "button"
                , Attribute.attribute "data-toggle" "dropdown"
                ]
                [ Html.span [ Attribute.class "glyphicon glyphicon-pencil" ] [] ]
            , Html.ul [ Attribute.class "dropdown-menu" ]
                (List.map (settingsButtonsControls controlId) controlTypes)
            ]
        , Html.button
            [ Attribute.class "btn btn-danger"
            , Attribute.type_ "button"
            , Event.onClick <| Data.DeleteControl controlId
            ]
            [ Html.span [ Attribute.class "glyphicon glyphicon-remove" ] [] ]
        ]


settingsButtonsControls : Data.ControlId -> ( Data.ControlType, String ) -> Html.Html Msg
settingsButtonsControls controlId ( controlType, name ) =
    Html.li
        [ Attribute.class "setting-button"
        , Event.onClick <| Data.ChangeControl controlId controlType
        ]
        [ Html.text name ]
