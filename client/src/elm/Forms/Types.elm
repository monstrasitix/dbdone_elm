module Forms.Types exposing (..)

-- Forms Messages


type Msg
    = AddControl
    | SelectControl String
    | ToggleEditing ControlId
    | UpdateLabelName ControlId String
    | UpdateIsRequired ControlId Bool
    | UpdateMinimumValue ControlId String
    | UpdateMaximumValue ControlId String
    | ChangeControl ControlId ControlType
    | DeleteControl ControlId



-- Available Controls


type ControlType
    = Text TextSettings
    | Number NumberSettings
    | Dropdown DropdownSettings



-- Forms Model Schema


type alias Model =
    { selectedControl : String
    , controlTypes : List ( ControlType, String )
    , controls : List Control
    }



-- Something For Clarity


type alias ControlId =
    Int


type alias Control =
    ( ControlId, ControlType )



-- Settings For Provided Controls


type alias TextSettings =
    { isEditing : Bool
    , labelName : String
    , isRequired : Bool
    , controlValue : String
    }


type alias NumberSettings =
    { isEditing : Bool
    , labelName : String
    , maximumValue : Int
    , minimumValue : Int
    , isRequired : Bool
    , controlValue : String
    }


type alias DropdownSettings =
    { isEditing : Bool
    , labelName : String
    , options : List String
    }



-- Forms Model


model : Model
model =
    { selectedControl = "Text Control"
    , controls = []
    , controlTypes =
        [ ( Text textDefault, "Text Control" )
        , ( Number numberDefault, "Number Control" )
        , ( Dropdown dropdownDefault, "Dropdown Control" )
        ]
    }



-- ControlDefaults


textDefault : TextSettings
textDefault =
    { isEditing = False
    , labelName = "Text Field"
    , isRequired = False
    , controlValue = ""
    }


numberDefault : NumberSettings
numberDefault =
    { isEditing = False
    , labelName = "Number Field"
    , isRequired = False
    , minimumValue = -100
    , maximumValue = 100
    , controlValue = ""
    }


dropdownDefault : DropdownSettings
dropdownDefault =
    { isEditing = False
    , labelName = "Dropdown Field"
    , options = ["Value 1", "Value 2", "Value 3"]
    }
