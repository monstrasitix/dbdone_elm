module Structure.Category.Update exposing (..)

import Structure.Category.Types as Data exposing (Model, Msg)
import Navigation


-- Structure Category Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Redirect To Categories
        Data.Redirect id ->
            ( model, Navigation.newUrl ("#records/" ++ (toString id)) )

        -- Updating Category Name
        Data.CategoryNameInput input ->
            ( model |> updateNameInput input
            , Cmd.none
            )

        -- Updating Search Field
        Data.SearchInput input ->
            case String.isEmpty input of
                True ->
                    ( model
                        |> updateSearchInput input
                        |> updateFilteredCategories model.categories
                    , Cmd.none
                    )

                False ->
                    ( model
                        |> updateSearchInput input
                        |> updateFilteredCategories
                            (filterByName input model.categories)
                    , Cmd.none
                    )

        -- Deleting A Category
        Data.DeleteCategory categoryId ->
            let
                -- Category Removed
                updatedList : List Data.Category
                updatedList =
                    removeCategory categoryId model.categories
            in
                ( model
                    |> updateCategories updatedList
                    |> updateFilteredCategories updatedList
                , Cmd.none
                )

        -- Adding A Category
        Data.AddCategory value ->
            let
                categoryName : String
                categoryName =
                    if String.isEmpty value then
                        "Default"
                    else
                        value

                -- Creating A New Storage
                newCategory : Data.Category
                newCategory =
                    Data.Category
                        ((+) 1 <| List.length model.categories)
                        categoryName
                        "25/03/2017"
                        False

                -- Adding Storage To The List
                updatedList : List Data.Category
                updatedList =
                    newCategory :: model.categories
            in
                ( model
                    |> updateCategories updatedList
                    |> updateFilteredCategories updatedList
                    |> updateNameInput ""
                , Cmd.none
                )

        -- Editing A Category
        Data.EditCategory category ->
            case category.editing of
                True ->
                    let
                        -- Category's Name Being Saved
                        updatedList : List Data.Category
                        updatedList =
                            updateCategoryName category.id model.nameInput model.categories
                    in
                        ( model
                            |> updateCategories updatedList
                            |> updateFilteredCategories
                                (filterByName model.search updatedList)
                        , Cmd.none
                        )

                False ->
                    let
                        -- Categoryies Not Being Edited
                        updatedList : List Data.Category
                        updatedList =
                            model.categories
                                |> List.map (toggleEdit category.id)
                    in
                        ( model
                            |> updateCategories updatedList
                            |> updateNameInput category.name
                            |> updateFilteredCategories
                                (filterByName model.search updatedList)
                        , Cmd.none
                        )



-- Update Name Input


updateNameInput : String -> Model -> Model
updateNameInput input model =
    { model | nameInput = input }



-- Update Search Input


updateSearchInput : String -> Model -> Model
updateSearchInput input model =
    { model | search = input }



-- Updated Filtered Categories


updateFilteredCategories : List Data.Category -> Model -> Model
updateFilteredCategories category model =
    { model | filteredCategories = category }



-- Update Categories


updateCategories : List Data.Category -> Model -> Model
updateCategories categoryList model =
    { model | categories = categoryList }



-- removing 1 Category


removeCategory : Int -> List Data.Category -> List Data.Category
removeCategory id categories =
    categories
        |> List.filter (\category -> category.id /= id)



-- Filtering By Category's Name


filterByName : String -> List Data.Category -> List Data.Category
filterByName name categories =
    categories
        |> List.filter
            (\category ->
                String.startsWith (String.toLower name) (String.toLower category.name)
            )



-- Updating Category's Name


updateCategoryName : Int -> String -> List Data.Category -> List Data.Category
updateCategoryName id value categories =
    List.map (updateName id value) categories



-- Updated Category's Name


updateName : Int -> String -> Data.Category -> Data.Category
updateName categoryId input category =
    let
        value : String
        value =
            if input == "" then
                category.name
            else
                input
    in
        if category.id == categoryId then
            { category
                | editing = False
                , name = value
            }
        else
            category



-- Toggle Category's Editing


toggleEdit : Int -> Data.Category -> Data.Category
toggleEdit categoryId category =
    if category.id == categoryId then
        { category | editing = True }
    else
        { category | editing = False }
