module Structure.Category.View exposing (..)

import Structure.Category.Types as Data exposing (Model, Msg)
import Html
import Html.Events as Event
import Html.Attributes as Attribute


-- Structure Category View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "container padding-top" ]
        [ viewSearch model
        , renderCategories model
        , modalWindow model
        ]


modalWindow : Model -> Html.Html Msg
modalWindow model =
    Html.div
        [ Attribute.class "modal fade"
        , Attribute.attribute "tabindex" "-1"
        , Attribute.attribute "role" "dialog"
        , Attribute.id "myModal"
        ]
        [ Html.div [ Attribute.class "modal-dialog" ]
            [ Html.div [ Attribute.class "modal-content" ]
                [ Html.div [ Attribute.class "modal-header" ]
                    [ Html.button
                        [ Attribute.type_ "button"
                        , Attribute.class "close"
                        , Attribute.attribute "data-dismiss" "modal"
                        , Attribute.attribute "aria-label" "Close"
                        , Attribute.attribute "aria-hidden" "true"
                        ]
                        [ Html.span [ Attribute.class "glyphicon glyphicon-remove" ] []
                        ]
                    , Html.h4 [ Attribute.class "modal-title" ]
                        [ Html.text "Enter Category's Name" ]
                    ]
                , Html.div [ Attribute.class "modal-body" ]
                    [ Html.form [ Attribute.class "col-xs-6 col-xs-offset-3" ]
                        [ Html.div [ Attribute.class "form-group" ]
                            [ Html.label []
                                [ Html.text "Name" ]
                            , Html.input
                                [ Attribute.class "form-control"
                                , Attribute.type_ "text"
                                , Attribute.value model.nameInput
                                , Event.onInput Data.CategoryNameInput
                                ]
                                []
                            ]
                        ]
                    , Html.div [ Attribute.class "clearfix" ] []
                    ]
                , Html.div [ Attribute.class "modal-footer" ]
                    [ Html.button
                        [ Attribute.type_ "button"
                        , Attribute.class "btn btn-default"
                        , Attribute.attribute "data-dismiss" "modal"
                        ]
                        [ Html.text "Close" ]
                    , Html.button
                        [ Attribute.type_ "button"
                        , Attribute.class "btn btn-default"
                        , Attribute.attribute "data-dismiss" "modal"
                        , Event.onClick <| Data.AddCategory model.nameInput
                        ]
                        [ Html.text "Done" ]
                    ]
                ]
            ]
        ]



-- Searching By Name


viewSearch : Model -> Html.Html Msg
viewSearch model =
    Html.div [ Attribute.class "row" ]
        [ Html.form []
            [ Html.div [ Attribute.class "form-group col-xs-12 flex flex-between" ]
                [ Html.span [ Attribute.class "glyphicon glyphicon-search" ] []
                , Html.input
                    [ Attribute.class "form-control component-input"
                    , Attribute.type_ "text"
                    , Attribute.value model.search
                    , Event.onInput Data.SearchInput
                    ]
                    []
                , Html.button
                    [ Attribute.class "btn btn-default"
                    , Attribute.type_ "button"
                    , Attribute.attribute "data-toggle" "modal"
                    , Attribute.attribute "data-target" "#myModal"
                    ]
                    [ Html.text "Add Category" ]
                ]
            ]
        ]



-- Renders Categories If They Exist


renderCategories : Model -> Html.Html Msg
renderCategories model =
    case List.isEmpty model.categories of
        False ->
            case List.isEmpty model.filteredCategories of
                False ->
                    Html.div [ Attribute.class "row" ]
                        (List.map (renderCategory model.nameInput) model.filteredCategories)

                True ->
                    Html.div [ Attribute.class "row" ]
                        [ Html.div [ Attribute.class "well well-lg text-center" ]
                            [ Html.text "No matches were found" ]
                        ]

        True ->
            Html.div [ Attribute.class "row" ]
                [ Html.div [ Attribute.class "well well-lg text-center" ]
                    [ Html.text "You have no categories created" ]
                ]



-- Renders A Category


renderCategory : String -> Data.Category -> Html.Html Msg
renderCategory value category =
    Html.div [ Attribute.class "col-xs-6" ]
        [ Html.div [ Attribute.class "panel panel-default" ]
            [ Html.div [ Attribute.class "panel-body storage" ]
                [ Html.p [ Attribute.class "lead flex flex-between" ]
                    [ categoryDisplay value category
                    , categoryActions category
                    ]
                ]
            , Html.div [ Attribute.class "darker panel-body" ]
                [ Html.text category.date
                , Html.button
                    [ Attribute.class "btn btn-default btn-default btn-sm pull-right"
                    , Event.onClick <| Data.Redirect category.id
                    ]
                    [ Html.text "Open" ]
                ]
            ]
        ]



-- Displays Category's Name


categoryDisplay : String -> Data.Category -> Html.Html Msg
categoryDisplay value category =
    case category.editing of
        True ->
            Html.input
                [ Attribute.class "form-control component-input"
                , Attribute.type_ "text"
                , Attribute.value value
                , Event.onInput Data.CategoryNameInput
                ]
                []

        False ->
            Html.text category.name



-- Category Buttons


categoryActions : Data.Category -> Html.Html Msg
categoryActions category =
    Html.div [ Attribute.class "dropdown pull-right" ]
        [ Html.button
            [ Attribute.class "btn btn-default btn-sm dropdown-toggle"
            , Attribute.type_ "button"
            , Attribute.attribute "data-toggle" "dropdown"
            ]
            [ Html.span [ Attribute.class "caret" ] [] ]
        , Html.ul [ Attribute.class "dropdown-menu" ]
            [ Html.li []
                [ Html.a
                    [ Event.onClick <| Data.EditCategory category
                    ]
                    [ Html.text <| renamingEditButton category.editing ]
                , Html.a
                    [ Event.onClick <| Data.DeleteCategory category.id ]
                    [ Html.text "Delete" ]
                ]
            ]
        ]



-- Renaming The Edit Button


renamingEditButton : Bool -> String
renamingEditButton isEditing =
    if isEditing then
        "Save"
    else
        "Rename"
