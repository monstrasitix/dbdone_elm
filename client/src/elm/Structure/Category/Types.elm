module Structure.Category.Types exposing (..)

-- Structure Category Messages


type Msg
    = EditCategory Category
    | AddCategory String
    | DeleteCategory Int
    | CategoryNameInput String
    | SearchInput String
    | Redirect Int



-- Structure Category Model Schema


type alias Model =
    { categories : List Category
    , filteredCategories : List Category
    , nameInput : String
    , search : String
    }



-- Category Model Schema


type alias Category =
    { id : Int
    , name : String
    , date : String
    , editing : Bool
    }



-- Structure Category Model


model : Model
model =
    { categories =
        [ Category 1 "Restaurant" "24/02/2017" False
        , Category 2 "Farm" "24/02/2017" False
        , Category 3 "People" "24/02/2017" False
        , Category 4 "Something Else" "24/02/2017" False
        ]
    , filteredCategories =
        [ Category 1 "Restaurant" "24/02/2017" False
        , Category 2 "Farm" "24/02/2017" False
        , Category 3 "People" "24/02/2017" False
        , Category 4 "Something Else" "24/02/2017" False
        ]
    , nameInput = ""
    , search = ""
    }
