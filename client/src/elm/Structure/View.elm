module Structure.View exposing (..)

import Structure.Types as Data exposing (Model, Msg)
import Structure.Storage.View as Storage
import Structure.Category.View as Category
import Routing
import Html


-- Structure View Function


view : Model -> Routing.Structure -> Html.Html Msg
view model route =
    Html.div []
        [ page model route
        ]



-- Render View By Route


page : Model -> Routing.Structure -> Html.Html Msg
page model route =
    case route of
        -- Structure Storage Route
        Routing.Route_Storage ->
            Html.map Data.StorageMessage <|
                Storage.view model.storageModel

        -- Structure Category Route
        Routing.Route_Category ->
            Html.map Data.CategoryMessage <|
                Category.view model.categoryModel
