module Structure.Storage.Types exposing (..)

-- Structure Storage Messages


type Msg
    = EditStorage Storage
    | AddStorage String
    | DeleteStorage Int
    | StorageNameInput String
    | SearchInput String
    | Redirect Int



-- Structure Storage Model Schema


type alias Model =
    { storages : List Storage
    , filteredStorages : List Storage
    , nameInput : String
    , search : String
    }



-- Structure Storage Model


model : Model
model =
    { storages =
        [ Storage 1 "Restaurant" "24/02/2017" False
        , Storage 2 "Farm" "24/02/2017" False
        , Storage 3 "People" "24/02/2017" False
        , Storage 4 "Something Else" "24/02/2017" False
        ]
    , filteredStorages =
        [ Storage 1 "Restaurant" "24/02/2017" False
        , Storage 2 "Farm" "24/02/2017" False
        , Storage 3 "People" "24/02/2017" False
        , Storage 4 "Something Else" "24/02/2017" False
        ]
    , nameInput = ""
    , search = ""
    }



-- Storage Model Schema


type alias Storage =
    { id : Int
    , name : String
    , date : String
    , editing : Bool
    }
