module Structure.Storage.View exposing (..)

import Structure.Storage.Types as Data exposing (Model, Msg)
import Html
import Html.Events as Event
import Html.Attributes as Attribute


-- Structure Storage View Function


view : Model -> Html.Html Msg
view model =
    Html.div [ Attribute.class "container padding-top" ]
        [ viewSearch model
        , renderStorages model
        , modalWindow model
        ]


modalWindow : Model -> Html.Html Msg
modalWindow model =
    Html.div
        [ Attribute.class "modal fade"
        , Attribute.attribute "tabindex" "-1"
        , Attribute.attribute "role" "dialog"
        , Attribute.id "myModal"
        ]
        [ Html.div [ Attribute.class "modal-dialog" ]
            [ Html.div [ Attribute.class "modal-content" ]
                [ Html.div [ Attribute.class "modal-header" ]
                    [ Html.button
                        [ Attribute.type_ "button"
                        , Attribute.class "close"
                        , Attribute.attribute "data-dismiss" "modal"
                        , Attribute.attribute "aria-label" "Close"
                        , Attribute.attribute "aria-hidden" "true"
                        ]
                        [ Html.span [ Attribute.class "glyphicon glyphicon-remove" ] []
                        ]
                    , Html.h4 [ Attribute.class "modal-title" ]
                        [ Html.text "Enter Storage's Name" ]
                    ]
                , Html.div [ Attribute.class "modal-body" ]
                    [ Html.form [ Attribute.class "col-xs-6 col-xs-offset-3" ]
                        [ Html.div [ Attribute.class "form-group" ]
                            [ Html.label []
                                [ Html.text "Name" ]
                            , Html.input
                                [ Attribute.class "form-control"
                                , Attribute.type_ "text"
                                , Attribute.value model.nameInput
                                , Event.onInput Data.StorageNameInput
                                ]
                                []
                            ]
                        ]
                    , Html.div [Attribute.class "clearfix"][]
                    ]
                , Html.div [ Attribute.class "modal-footer" ]
                    [ Html.button
                        [ Attribute.type_ "button"
                        , Attribute.class "btn btn-default"
                        , Attribute.attribute "data-dismiss" "modal"
                        ]
                        [ Html.text "Close" ]
                    , Html.button
                        [ Attribute.type_ "button"
                        , Attribute.class "btn btn-default"
                        , Attribute.attribute "data-dismiss" "modal"
                        , Event.onClick <| Data.AddStorage model.nameInput
                        ]
                        [ Html.text "Done" ]
                    ]
                ]
            ]
        ]



-- Searching By Name


viewSearch : Model -> Html.Html Msg
viewSearch model =
    Html.div [ Attribute.class "row" ]
        [ Html.form []
            [ Html.div [ Attribute.class "form-group col-xs-12 flex flex-between" ]
                [ Html.span [ Attribute.class "glyphicon glyphicon-search" ] []
                , Html.input
                    [ Attribute.class "form-control component-input"
                    , Attribute.type_ "text"
                    , Attribute.value model.search
                    , Event.onInput Data.SearchInput
                    ]
                    []
                , Html.button
                    [ Attribute.class "btn btn-default"
                    , Attribute.type_ "button"
                    , Attribute.attribute "data-toggle" "modal"
                    , Attribute.attribute "data-target" "#myModal"
                    ]
                    [ Html.text "Add Storage" ]
                ]
            ]
        ]



-- Renders Storages If They Exist


renderStorages : Model -> Html.Html Msg
renderStorages model =
    case List.isEmpty model.storages of
        False ->
            case List.isEmpty model.filteredStorages of
                False ->
                    Html.div [ Attribute.class "row" ]
                        (List.map (renderStorage model.nameInput) model.filteredStorages)

                True ->
                    Html.div [ Attribute.class "row" ]
                        [ Html.div [ Attribute.class "well well-lg text-center" ]
                            [ Html.text "No matches were found" ]
                        ]

        True ->
            Html.div [ Attribute.class "row" ]
                [ Html.div [ Attribute.class "well well-lg text-center" ]
                    [ Html.text "You have no storages created" ]
                ]



-- Renders A Storage


renderStorage : String -> Data.Storage -> Html.Html Msg
renderStorage value storage =
    Html.div [ Attribute.class "col-xs-4" ]
        [ Html.div [ Attribute.class "panel panel-default" ]
            [ Html.div [ Attribute.class "panel-body storage" ]
                [ Html.p [ Attribute.class "lead flex flex-between" ]
                    [ storageDisplay value storage
                    , storageActions storage
                    ]
                ]
            , Html.div [ Attribute.class "darker panel-body" ]
                [ Html.text storage.date
                , Html.button
                    [ Attribute.class "btn btn-default btn-default btn-sm pull-right"
                    , Event.onClick <| Data.Redirect storage.id
                    ]
                    [ Html.text "Open" ]
                ]
            ]
        ]



-- Displays Storage's Name


storageDisplay : String -> Data.Storage -> Html.Html Msg
storageDisplay value storage =
    case storage.editing of
        True ->
            Html.input
                [ Attribute.class "form-control component-input"
                , Attribute.type_ "text"
                , Attribute.value value
                , Event.onInput Data.StorageNameInput
                ]
                []

        False ->
            Html.text storage.name



-- Storage Buttons


storageActions : Data.Storage -> Html.Html Msg
storageActions storage =
    Html.div [ Attribute.class "dropdown pull-right" ]
        [ Html.button
            [ Attribute.class "btn btn-default btn-sm dropdown-toggle"
            , Attribute.type_ "button"
            , Attribute.attribute "data-toggle" "dropdown"
            ]
            [ Html.span [ Attribute.class "caret" ] [] ]
        , Html.ul [ Attribute.class "dropdown-menu" ]
            [ Html.li []
                [ Html.a
                    [ Event.onClick <| Data.EditStorage storage
                    ]
                    [ Html.text <| renamingEditButton storage.editing ]
                , Html.a
                    [ Event.onClick <| Data.DeleteStorage storage.id ]
                    [ Html.text "Delete" ]
                ]
            ]
        ]



-- Renaming The Edit Button


renamingEditButton : Bool -> String
renamingEditButton isEditing =
    if isEditing then
        "Save"
    else
        "Rename"
