module Structure.Storage.Update exposing (..)

import Navigation
import Structure.Storage.Types as Data exposing (Model, Msg)


-- Structure Storage Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Redirect To Categories
        Data.Redirect id ->
            ( model, Navigation.newUrl ("#categories/" ++ (toString id)) )

        -- Update Storage Name Field
        Data.StorageNameInput input ->
            ( model |> updateNameInput input
            , Cmd.none
            )

        -- Update Search Field
        Data.SearchInput input ->
            case String.isEmpty input of
                True ->
                    ( model
                        |> updateSearchInput input
                        |> updateFilteredStorages model.storages
                    , Cmd.none
                    )

                False ->
                    ( model
                        |> updateSearchInput input
                        |> updateFilteredStorages
                            (filterByName input model.storages)
                    , Cmd.none
                    )

        -- Delete A Storage With A Specified Id
        Data.DeleteStorage storageId ->
            let
                -- Remove One Storages With Matching IDs
                updatedList : List Data.Storage
                updatedList =
                    removeStorage storageId model.storages
            in
                ( model
                    |> updateStorages updatedList
                    |> updateFilteredStorages updatedList
                , Cmd.none
                )

        -- Adds Storage To The Collection
        Data.AddStorage value ->
            let
                storageName : String
                storageName =
                    if String.isEmpty value then
                        "Default"
                    else
                        value

                -- Creating A New Storage
                newStorage : Data.Storage
                newStorage =
                    Data.Storage
                        ((+) 1 <| List.length model.storages)
                        storageName
                        "25/03/2017"
                        False

                -- Adding Storage To The List
                updatedList : List Data.Storage
                updatedList =
                    newStorage :: model.storages
            in
                ( model
                    |> updateStorages updatedList
                    |> updateFilteredStorages updatedList
                    |> updateNameInput ""
                , Cmd.none
                )

        -- Storage Name Being Edited
        Data.EditStorage storage ->
            case storage.editing of
                True ->
                    let
                        -- Updating The Name For A PArticular Storge
                        updatedList : List Data.Storage
                        updatedList =
                            updateStorageName storage.id model.nameInput model.storages
                    in
                        ( model
                            |> updateStorages updatedList
                            |> updateFilteredStorages
                                (filterByName model.search updatedList)
                        , Cmd.none
                        )

                False ->
                    let
                        -- Untoggling All Storages
                        updatedList : List Data.Storage
                        updatedList =
                            model.storages
                                |> List.map (toggleEdit storage.id)
                    in
                        ( model
                            |> updateStorages updatedList
                            |> updateNameInput storage.name
                            |> updateFilteredStorages
                                (filterByName model.search updatedList)
                        , Cmd.none
                        )



-- Update Name Input


updateNameInput : String -> Model -> Model
updateNameInput input model =
    { model | nameInput = input }



-- Update Search Input


updateSearchInput : String -> Model -> Model
updateSearchInput input model =
    { model | search = input }



-- Update Filtered Storages


updateFilteredStorages : List Data.Storage -> Model -> Model
updateFilteredStorages storages model =
    { model | filteredStorages = storages }



-- Update Storages


updateStorages : List Data.Storage -> Model -> Model
updateStorages storageList model =
    { model | storages = storageList }



-- Remove Storage


removeStorage : Int -> List Data.Storage -> List Data.Storage
removeStorage id storages =
    storages
        |> List.filter (\storage -> storage.id /= id)



-- Search Storages By Name


filterByName : String -> List Data.Storage -> List Data.Storage
filterByName name storages =
    storages
        |> List.filter
            (\storage ->
                String.startsWith (String.toLower name) (String.toLower storage.name)
            )



-- Toggle Storage's Editing


toggleEdit : Int -> Data.Storage -> Data.Storage
toggleEdit storageId storage =
    if storage.id == storageId then
        { storage | editing = True }
    else
        { storage | editing = False }



-- Update Storage's Name


updateStorageName : Int -> String -> List Data.Storage -> List Data.Storage
updateStorageName id value storages =
    List.map (updateName id value) storages



-- Updated Storage's Name


updateName : Int -> String -> Data.Storage -> Data.Storage
updateName storageId input storage =
    let
        value : String
        value =
            if input == "" then
                storage.name
            else
                input
    in
        if storage.id == storageId then
            { storage
                | editing = False
                , name = value
            }
        else
            storage
