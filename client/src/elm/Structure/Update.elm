module Structure.Update exposing (..)

import Structure.Types as Data exposing (Model, Msg)
import Structure.Storage.Update as Storage
import Structure.Category.Update as Category


-- Structure Update Function


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Bind Structure Storage
        Data.StorageMessage message ->
            let
                ( newModel, newCmd ) =
                    Storage.update message model.storageModel
            in
                ( { model | storageModel = newModel }
                , Cmd.map Data.StorageMessage newCmd
                )

        -- Bind Structure Category
        Data.CategoryMessage message ->
            let
                ( newModel, newCmd ) =
                    Category.update message model.categoryModel
            in
                ( { model | categoryModel = newModel }
                , Cmd.map Data.CategoryMessage newCmd
                )
