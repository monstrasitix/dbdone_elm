module Structure.Types exposing (..)

import Structure.Storage.Types as Storage
import Structure.Category.Types as Category


-- Structure Messages


type Msg
    = StorageMessage Storage.Msg
    | CategoryMessage Category.Msg



-- Structure Model Schema


type alias Model =
    { storageModel : Storage.Model
    , categoryModel : Category.Model
    }



-- Structure Model


model : Model
model =
    { storageModel = Storage.model
    , categoryModel = Category.model
    }
