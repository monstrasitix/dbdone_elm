module Types exposing (..)

import Navigation
import Routing
import Navbar.Types as Navbar
import Home.Types as Home
import Authentication.Types as Authentication
import Dashboard.Types as Dashboard
import Structure.Types as Structure
import Records.Types as Records


-- Main Messages


type Msg
    = OnLocationChange Navigation.Location
    | DashboardMessage Dashboard.Msg
    | NavbarMessage Navbar.Msg
    | HomeMessage Home.Msg
    | AuthenticationMessage Authentication.Msg
    | StructureMessage Structure.Msg
    | RecordsMessage Records.Msg



-- Main Model Schema


type alias Model =
    { route : Routing.Route
    , dashboardModel : Dashboard.Model
    , navbarModel : Navbar.Model
    , homeModel : Home.Model
    , authenticationModel : Authentication.Model
    , structureModel : Structure.Model
    , recordsModel : Records.Model
    }



-- Main Model


model : Routing.Route -> Model
model route =
    { route = route
    , dashboardModel = Dashboard.model
    , navbarModel = Navbar.model
    , homeModel = Home.model
    , structureModel = Structure.model
    , authenticationModel = Authentication.model
    , recordsModel = Records.model
    }
