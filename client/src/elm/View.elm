module View exposing (..)

import Types as Data exposing (Model, Msg)
import Routing
import Html
import Home.View as Home
import Navbar.View as Navbar
import Authentication.View as Authentication
import Dashboard.View as Dashboard
import Structure.View as Structure
import Errors.View as Errors
import Records.View as Records


-- Main View Function


view : Model -> Html.Html Msg
view model =
    Html.div []
        [ Html.map Data.NavbarMessage <|
            Navbar.view model.navbarModel
        , page model
        ]



-- Rendering Views By Route


page : Model -> Html.Html Msg
page model =
    case model.route of
        -- Home Route
        Routing.Route_Home ->
            Html.map Data.HomeMessage <|
                Home.view model.homeModel

        -- Dashboard Route
        Routing.Route_Dashboard ->
            Html.map Data.DashboardMessage <|
                Dashboard.view model.dashboardModel

        -- Authentication Login Route
        Routing.Route_Authentication_Login ->
            Html.map Data.AuthenticationMessage <|
                Authentication.view model.authenticationModel Routing.Route_Login

        -- Authentication Register Route
        Routing.Route_Authentication_Register ->
            Html.map Data.AuthenticationMessage <|
                Authentication.view model.authenticationModel Routing.Route_Register

        -- Structure Storage Route
        Routing.Route_Structure_Storage userId ->
            Html.map Data.StructureMessage <|
                Structure.view model.structureModel Routing.Route_Storage

        -- Structure Category Route
        Routing.Route_Structure_Category storageId ->
            Html.map Data.StructureMessage <|
                Structure.view model.structureModel Routing.Route_Category

        Routing.Route_Records categoryId ->
            Html.map Data.RecordsMessage <|
                Records.view model.recordsModel

        -- Not Found Route
        Routing.Route_None ->
            Errors.view_404
