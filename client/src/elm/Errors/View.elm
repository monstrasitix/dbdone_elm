module Errors.View exposing (..)

import Html
import Html.Attributes as Attribute


-- Error 404


view_404 : Html.Html msg
view_404 =
    Html.div [ Attribute.class "container padding-top" ]
        [ Html.div [ Attribute.class "well well-lg text-center" ]
            [ Html.h2 []
                [ Html.text "404" ]
            , Html.p []
                [ Html.text "Page Not Found" ]
            ]
        ]
