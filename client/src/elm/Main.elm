module Main exposing (..)

import Types as Data exposing (Model, Msg)
import Navigation
import Routing
import Update
import View


-- Main Function


main : Program Never Model Msg
main =
    Navigation.program Data.OnLocationChange
        { init = init
        , update = Update.update
        , view = View.view
        , subscriptions = subscriptions
        }



-- Initializing


init : Navigation.Location -> ( Model, Cmd Msg )
init location =
    let
        -- Parsing The Route
        route : Routing.Route
        route =
            Routing.parseLocation location
    in
        ( Data.model route, Cmd.none )



-- Subscriptions Function


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
