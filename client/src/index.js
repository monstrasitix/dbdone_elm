'use strict';

require('./index.html');
require('./css/main.sass');

var Elm = require('./elm/Main.elm');
var app = Elm.Main.embed(document.getElementById('app'));